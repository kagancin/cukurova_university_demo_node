import * as express from 'express';
import * as io from 'socket.io';
import * as http from 'http';
import * as cors from 'cors';

import { Game } from './game';

const app = express();


app.use(cors());


const server = http.createServer(app);

const ioServer = io(server, { path: '/' });

const playerNamespace = ioServer.of('/player');
const adminNamespace = ioServer.of('/admin');

const PORT = process.env.PORT || 4000;

const game = new Game(playerNamespace, adminNamespace);

server.listen(PORT, () => { console.log('Listening on port %s...', PORT) });
