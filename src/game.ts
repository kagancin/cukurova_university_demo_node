import { EVENTS } from './events';
import { Player } from './player';

export class Game {
	private players: Player[] = [];
	private magicNumber: number;

	private PLAYER_CHANCE = 5

	constructor(private ioPlayer: SocketIO.Namespace, private ioAdmin: SocketIO.Namespace) {
		ioPlayer.on(EVENTS.CONNECTION, (socket: SocketIO.Socket) => { this.connected(socket); });
		ioAdmin.on(EVENTS.CONNECTION, (socket: SocketIO.Socket) => { this.adminConnected(socket); });
	}

	private start(magicNumber?: number) {
		if (!this.magicNumber) {
			this.magicNumber = magicNumber ? magicNumber : Math.floor(Math.random() * 100);
			this.gameStateChanged(true);
			this.setChances(this.PLAYER_CHANCE);
		}

		console.log('Magic: ', this.magicNumber);
	}

	private end(winner?: Player, answer?: number) {
		if (this.magicNumber) {
			this.ioPlayer.emit(EVENTS.GAME_STATE_CHANGED, false);
			this.magicNumber = undefined;
			this.gameStateChanged(false, winner, answer);
		}
	}

	private setChances(chance: number) {
		this.players.map((player: Player) => {
			player.chance = chance;
			this.ioPlayer.to(player.id).emit(EVENTS.PLAYER_STATE, player);
		});
	}

	private connected(socket: SocketIO.Socket) {
		this.gameStateChanged(this.magicNumber !== undefined);

		console.log('+', socket.id);

		socket.on(EVENTS.JOIN, (username, callback) => { this.join(socket, username, callback); });
		socket.on(EVENTS.GUESS, (guess, callback) => { this.guess(socket, guess, callback); });
		socket.on(EVENTS.DISCONNECT, () => { this.disconnect(socket) });
	}

	private disconnect(socket: SocketIO.Socket) {
		console.log('-', socket.id);
		if (this.doesPlayerExist(socket.id) === true) {
			this.removePlayer(socket.id);
		}
	}

	private join(socket: SocketIO.Socket, username: string, callback: Function) {
		if (!this.doesPlayerExist(socket.id, username)) {
			this.addPlayer(socket.id, username);
			callback(true);
			socket.emit(EVENTS.PLAYER_STATE, this.getPlayer(socket.id));
		} else {
			callback(false);
		}
	}

	private guess(socket: SocketIO.Socket, guess: number, callback: Function) {
		let player = this.getPlayer(socket.id);

		if (this.magicNumber === undefined) {
			callback('The game is off');
		} else if (player.chance <= 0) {
			callback('You cannot send guess anymore');
		} else {
			player.chance--;
			socket.emit(EVENTS.PLAYER_STATE, player);
			if (guess == this.magicNumber) {
				this.end(this.getPlayer(socket.id), this.magicNumber);
			} else if (guess > this.magicNumber) {
				callback(`The number is smaller than ${guess}`);
			} else if (guess < this.magicNumber) {
				callback(`The number is larger than ${guess}`);
			}
		}

	}

	private removePlayer(id: string) {
		this.players = this.players.filter((player: Player) => player.id !== id);
		this.playerCountChanged();
	}

	private addPlayer(id: string, username: string) {
		let player = new Player(id, username, this.PLAYER_CHANCE);
		this.players.push(player);
		this.playerCountChanged();
	}

	private getPlayer(id: string): Player | undefined {
		let filtered = this.players.filter((player: Player) => player.id === id);
		let player: Player = filtered.length ? filtered[0] : undefined;
		return filtered[0];
	}

	private playerCountChanged() {
		this.ioPlayer.emit(EVENTS.PLAYER_COUNT_CHANGED, this.players.length);
		this.ioAdmin.emit(EVENTS.PLAYER_COUNT_CHANGED, this.players.length);
	}

	private gameStateChanged(state: boolean, winner?: Player, answer?: number) {
		if (!winner && !answer) {
			this.ioPlayer.emit(EVENTS.GAME_STATE_CHANGED, state);
			this.ioAdmin.emit(EVENTS.GAME_STATE_CHANGED, state);
		} else {
			this.ioPlayer.emit(EVENTS.GAME_STATE_CHANGED, state, winner, answer);
			this.ioAdmin.emit(EVENTS.GAME_STATE_CHANGED, state, winner, answer);
		}
	}

	private doesPlayerExist(id: string, username?: string): boolean {
		return this.players.some((player: Player) => player.id === id || player.username === username);
	}

	private adminConnected(socket: SocketIO.Socket) {
		this.gameStateChanged(this.magicNumber !== undefined);
		this.playerCountChanged();

		console.log('**admin**', socket.id);

		socket.on(EVENTS.START, () => { this.start(); });
		socket.on(EVENTS.END, () => { this.end() });
	}


}
